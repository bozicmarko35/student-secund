import logo from '../logo.svg';
import React from 'react';
import './App.css';
import '../Student';
import Student from "../Student";
import raw from '../students.txt';
import { v4 as uuidv4 } from 'uuid';
class App extends React.Component {

    constructor() {
        super();
        this.state = {
            students: []
        }


        const student = new Student("iva", "engleska", "12345", "132456");
        const student1 = new Student("mara", "srbija", "78", "89");
        const student2 = new Student("test", "test", "test", "test");

        console.log(student);
        console.log(student1);
        console.log(student2);

        fetch(raw)
            .then(r => r.text())
            .then(text => {

                let name;
                let phone;
                let address;
                let courses;
                const splitter = text.split("\n");
                let counter = 0;
                for (let i = 0; i < splitter.length; i++) {

                    switch (counter) {
                        case 0:
                            name = splitter[i];
                            counter++;
                            break;
                        case 1:
                            address = splitter[i];
                            counter++;
                            break;
                        case 2:
                            phone = splitter[i];
                            counter++;
                            break;
                        case 3:
                            courses = splitter[i];
                            counter=0;
                            const array= this.state.students.concat(new Student(name,address,phone,courses));
                            this.setState({students: array});



                    }
                }



            });


    }

    render() {
        return (
            <div>

                {this.state.students.map(value => <h3>{value.getInfo()}</h3>)}

            </div>
        )
    }
}

export default App;
