class Student{
    _name;
    _address;
    _phone;
    _course;


    constructor(name, address, phone, course) {
        this._name = name;
        this._address = address;
        this._phone = phone;
        this._course = course;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get address() {
        return this._address;
    }

    set address(value) {
        this._address = value;
    }

    get phone() {
        return this._phone;
    }

    set phone(value) {
        this._phone = value;
    }

    get course() {
        return this._course;
    }

    set course(value) {
        this._course = value;
    }

    getInfo() {
        return "Name: " + this._name + ", Address: " + this._address + ", Phone: " + this._phone + ", Course: " + this._course;
    }

}
export default Student;